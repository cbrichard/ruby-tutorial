# Intro to Ruby

class Person
   # Getters  
   # attr_reader :name, :location

    def initialize(name, location)
        @name = name
        @location = location
    end
    
    def greet
        "Hello my name is #{@name} and I'm from #{@location}"
    end
end

chris = Person.new "Chris", "Santa Clara"
beauty = Person.new "Beauty", "San Francisco"


def print_message(person)
    "Hello my name is #{person.name} and I'm from #{person.location}"
end

# create array for people to output and use for loop to print the greet
people = [chris, beauty]

people.each do |person|
    puts person.greet
end

# Or Loop with lambda anonymous fucntion
# person_block = lambda {|person| puts person.greet }
# people.each(&person_block)
#
# Or
# for person in people do
#  puts person.greet
# end

# Reopen another class using existing class and inherit instance variables.
class Person
    def say_goodbye
        "I am going back to #{@location} now"
    end
end

puts chris.say_goodbye
puts beauty.say_goodbye


class Fixnum
    def print_age
        2015 - self
    end
end

age = 1982.print_age
puts "Chris's age is #{age}"
